## Implementation

* `composer require infotechnohelp/cakephp-auth-api`

* `App/config/bootstrap.php`

```
Plugin::load('AuthApi', ['routes' => true]);
```
or

`bin/cake plugin load AuthApi -r`

* `bin/cake migrations migrate --plugin AuthApi`

* `App/src/Controller/AppController.php`

`class AppController extends \AuthApi\Controller\AppController`

## Register a new user

`bin/cake AuthApi.users register <username> <password> <user_role_id>`

## Usage in controllers
By default, all controllers will have a forbidden access. 

In order to allow a controller's method:

```
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow('allowedMethod');
    }
```

## API

* APP/auth-api/register → username, password, repeat-password (email, user_role_id)

`{"data":entity, "message": null}`

`{"data":null, "message": message text}`

* APP/auth-api → username, password

`{"data":entity, "message": null}`

`{"data":null, "message": Error message text}`

* APP/auth-api/logout

`{"data":true, "message": null}`