<?php

namespace AuthApi\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;

class TestControllerTest extends IntegrationTestCase
{
    public function testAllowedAction()
    {
        $this->get('tests/allowed-action');
        $this->assertResponseOk();
    }
}
