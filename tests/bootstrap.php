<?php

declare(strict_types=1);

require dirname(__DIR__) . '/vendor/cakephp/cakephp/src/basics.php';
require dirname(__DIR__) . '/vendor/autoload.php';

if ( ! defined('WINDOWS')) {
    if (DS == '\\' || substr(PHP_OS, 0, 3) === 'WIN') {
        define('WINDOWS', true);
    } else {
        define('WINDOWS', false);
    }
}

define('PLUGIN_ROOT', dirname(__DIR__));
define('ROOT', dirname(__DIR__) . DS . 'tests' . DS . 'TestApp');
define('APP_DIR', 'src');

define('APP', ROOT . DS . APP_DIR . DS);
if ( ! is_dir(APP)) {
    mkdir(APP, 0770, true);
}

define('TMP', ROOT . DS . 'tmp' . DS);
if ( ! is_dir(TMP)) {
    mkdir(TMP, 0770, true);
}

define('CONFIG', ROOT . DS . 'config' . DS);
define('LOGS', ROOT . DS . 'logs' . DS);
define('CACHE', TMP . 'cache' . DS);

define('CAKE_CORE_INCLUDE_PATH', ROOT . '/vendor/cakephp/cakephp');
define('CORE_PATH', CAKE_CORE_INCLUDE_PATH . DS);
define('CAKE', CORE_PATH . 'src' . DS);

Cake\Core\Configure::write('App', [
    'namespace' => 'TestApp',
]);

Cake\Core\Configure::write('App.paths.templates', [APP . 'Template' . DS]);

Cake\Core\Configure::write('debug', true);

$cache = [
    'default'      => [
        'engine' => 'File',
        'path'   => CACHE,
    ],
    '_cake_core_'  => [
        'className' => 'File',
        'prefix'    => 'crud_myapp_cake_core_',
        'path'      => CACHE . 'persistent/',
        'serialize' => true,
        'duration'  => '+10 seconds',
    ],
    '_cake_model_' => [
        'className' => 'File',
        'prefix'    => 'crud_my_app_cake_model_',
        'path'      => CACHE . 'models/',
        'serialize' => 'File',
        'duration'  => '+10 seconds',
    ],
];

Cake\Cache\Cache::setConfig($cache);

$dotenv = new \Dotenv\Dotenv(PLUGIN_ROOT . DS . 'config');
$dotenv->overload();
$dotenv->required(['DB_TEST_HOST', 'DB_TEST_USERNAME', 'DB_TEST_PASSWORD', 'DB_TEST_NAME']);

Cake\Datasource\ConnectionManager::setConfig('test', [
    'className'        => 'Cake\Database\Connection',
    'driver'           => 'Cake\Database\Driver\Mysql',
    'persistent'       => false,
    'host'             => getenv('DB_TEST_HOST'),
    'username'         => getenv('DB_TEST_USERNAME'),
    'password'         => getenv('DB_TEST_PASSWORD'),
    'database'         => getenv('DB_TEST_NAME'),
    'encoding'         => 'utf8',
    'timezone'         => 'UTC',
    'flags'            => [],
    'cacheMetadata'    => true,
    'log'              => false,
    'quoteIdentifiers' => false,
]);

\Cake\Log\Log::setConfig('debug', [
    'className' => 'File',
    'path'      => LOGS,
    'levels'    => ['notice', 'info', 'debug'],
    'file'      => 'debug',
]);

\Cake\Log\Log::setConfig('error', [
    'className' => 'Cake\Log\Engine\FileLog',
    'path'      => LOGS,
    'levels'    => ['warning', 'error', 'critical', 'alert', 'emergency'],
    'file'      => 'error',
]);

\Cake\Routing\Router::reload();

\Cake\Core\Plugin::load('AuthApi', ['path' => dirname(dirname(__FILE__)) . DS, 'routes' => true]);