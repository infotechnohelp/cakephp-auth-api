<?php

namespace TestApp\Controller;

class TestsController extends AppController
{

    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow('allowedAction');
    }

    public function allowedAction()
    {
        return null;
    }

    public function forbiddenAction()
    {
        $this->autoRender = false;

        return null;
    }
}
