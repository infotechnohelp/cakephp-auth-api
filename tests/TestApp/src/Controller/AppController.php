<?php

namespace TestApp\Controller;

use Cake\Controller\Controller;

class AppController extends Controller
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('AuthApi.Authentication');
    }
}