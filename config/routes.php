<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'AuthApi',
    ['path' => '/auth-api'],
    function (RouteBuilder $routes) {
        $routes->prefix('api', function ($routes) {

            $routes->connect('/register', ['controller' => 'Users', 'action' => 'register']);
            $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
            $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);

            $routes->connect('/not-allowed', ['controller' => 'Errors', 'action' => 'not-allowed']);

            /** @var $routes RouteBuilder */
            $routes->fallbacks(DashedRoute::class);
        });

        $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
        $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);

        $routes->fallbacks(DashedRoute::class);
    }
);
