<?php

namespace AuthApi\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;

/**
 * Class UsersShell
 * @package App\Shell
 */
class UsersShell extends Shell
{
    /**
     * @var null
     */
    private $username = null;

    /**
     * @var null
     */
    private $password = null;

    /**
     * @var null
     */
    private $userRoleId = null;

    /**
     * @throws \Exception
     */
    public function startup()
    {
        if (count($this->args) == 3) {
            $this->username   = $this->args[0];
            $this->password   = $this->args[1];
            $this->userRoleId = $this->args[2];
        } else {
            throw new \Exception('Incorrect input');
        }

        parent::startup();
    }

    /**
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addArgument('command', [
            'help'     => 'Specify action',
            'required' => true,
        ]);

        $parser->addArgument('username', [
            'help'     => 'Provide username',
            'required' => true,
        ]);

        $parser->addArgument('password', [
            'help'     => 'Provide password',
            'required' => true,
        ]);

        $parser->addArgument('user_role_id', [
            'help'     => 'Provide user role id',
            'required' => true,
        ]);

        return $parser;
    }

    public function register()
    {
        $UsersTable = TableRegistry::getTableLocator()->get('AuthApi.Users');
        $result     = $UsersTable->save($UsersTable->newEntity([
            'username'     => $this->username,
            'password'     => $this->password,
            'user_role_id' => $this->userRoleId,
        ]));

        if (! $result) {
            $this->out('User registration was unsuccessful');
        } else {
            $this->out("User '" . $this->username . "' was successfully registered");
        }
    }
}
