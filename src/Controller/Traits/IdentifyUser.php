<?php

declare(strict_types=1);

namespace AuthApi\Controller\Traits;

/**
 * Trait IdentifyUser
 * @package AuthApi\Controller\Helper
 */
trait IdentifyUser
{
    /**
     * @return mixed
     */
    private function identifyUser()
    {
        $authenticateConfig = $this->Auth->getConfig('authenticate');

        if (filter_var($this->getRequest()->getData('username'), FILTER_VALIDATE_EMAIL) !== false) {
            $this->Auth->setConfig('authenticate', [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password'],
                ],
            ]);

            $this->setRequest($this->getRequest()->withData('email', $this->getRequest()->getData('username')));
        }


        $result = $this->Auth->identify();

        $this->Auth->setConfig('authenticate', $authenticateConfig);

        $this->Auth->constructAuthenticate();

        return $result;
    }
}
