<?php

declare(strict_types=1);

namespace AuthApi\Controller;

use App\Controller\AppController as BaseController;

/**
 * Class AppController
 * @package AuthApi\Controller
 */
class AppController extends BaseController
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}
