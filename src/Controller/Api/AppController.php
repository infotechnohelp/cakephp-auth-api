<?php

declare(strict_types=1);

namespace AuthApi\Controller\Api;

use App\Controller\Api\AppController as BaseController;

/**
 * Class AppController
 * @package AuthApi\Controller\Api
 */
class AppController extends BaseController
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}
