<?php

declare(strict_types=1);

namespace AuthApi\Controller\Api;

use AuthApi\Model\Entity\User;
use AuthApi\Model\Entity\UserRole;
use AuthApi\Controller\Traits\IdentifyUser;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{
    use IdentifyUser;

    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function login()
    {
        $user = $this->identifyUser();

        if ($user) {
            $this->Auth->setUser($user);

            $this->_setResponse($user);

            return;
        }

        throw new BadRequestException('User cannot be identified');
    }

    public function logout()
    {
        $this->Auth->logout();

        $this->_setResponse([true]);
    }

    public function register()
    {
        $UsersTable = TableRegistry::getTableLocator()->get('AuthApi.Users');
        $requestData = $this->_getRequestData();

        if (!isset($requestData['password']) || empty($requestData['password'])) {
            throw new BadRequestException('Please provide password');
        }

        if (!isset($requestData['repeat-password']) || empty($requestData['repeat-password'])) {
            throw new BadRequestException('Please repeat password');
        }

        if ($requestData['password'] !== $requestData['repeat-password']) {
            throw new BadRequestException('Passwords do not match');
        }

        $userRoleId = (empty($requestData['user_role_id'])) ? null : (int)$requestData['user_role_id'];

        if ($userRoleId !== UserRole::USER && $userRoleId !== null && !$this->Auth->user()) {
            throw new BadRequestException('You are not allowed to register a new user with this user role');
        }

        /** @var  User $User */
        $User = $UsersTable->newEntity(array_merge($requestData, [
            'user_role_id' => $userRoleId,
        ]));

        $result = $UsersTable->saveOrFail($User);

        $result->unsetProperty('repeat-password');

        $this->_setResponse($result);
    }

    /**
     * @param \Cake\Event\Event $event
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        $action = $event->getSubject()->getRequest()->getParam('action');

        if ($action === 'logout') {
            $this->request->allowMethod('get');
        } else {
            $this->request->allowMethod('post');
        }
    }
}
