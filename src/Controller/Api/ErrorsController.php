<?php

declare(strict_types=1);

namespace AuthApi\Controller\Api;

use Cake\Http\Exception\ForbiddenException;

class ErrorsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function notAllowed()
    {
        throw new ForbiddenException('Not allowed');
    }
}
