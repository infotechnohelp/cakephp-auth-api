<?php

declare(strict_types=1);

namespace AuthApi\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;

/**
 * Class LanguagesComponent
 * @package Languages\Controller\Component
 */
class AuthenticationComponent extends Component
{
    public function initialize(array $config = [])
    {
        parent::initialize($config);

        $controller = $this->_registry->getController();

        $authConfig = (empty(Configure::read('AuthApi'))) ?
            [
                'loginAction' => '/auth-api/api/not-allowed',
                'authError' => false,
            ] :
            Configure::read('AuthApi');

        $authConfig = ($authConfig === 'default') ? [] : $authConfig;

        $controller->loadComponent('Auth', $authConfig);
    }
}
