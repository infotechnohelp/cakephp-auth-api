<?php
namespace AuthApi\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * Class User
 * @package UserAuth\Model\Entity
 */
class User extends Entity
{

    /**
     * @var array
     */
    //@codingStandardsIgnoreStart
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];


    /**
     * @param $password
     *
     * @return bool|string
     */
    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher())->hash($password);
    }
    //@codingStandardsIgnoreEnd
}
