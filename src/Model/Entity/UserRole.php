<?php

namespace AuthApi\Model\Entity;

use App\Model\Entity\UserRole as BaseEntity;

class UserRole extends BaseEntity
{
    //@codingStandardsIgnoreStart

    /**
     * @var array
     */
    protected $_accessible = [
        '*'  => true,
        'id' => false,
    ];
    //@codingStandardsIgnoreEnd
}
